![alt text](http://www.mustangx.org/mx-imgz/logo/MX_LOGO_v1.1_CMS-black_300-159.png "MustangX Logo")

[![build status](https://gitlab.com/HubShark/MustangX/badges/master/build.svg)](https://gitlab.com/HubShark/MustangX/commits/master)
[![coverage report](https://gitlab.com/HubShark/MustangX/badges/master/coverage.svg)](https://gitlab.com/HubShark/MustangX/commits/master)

Building a _**CMS from scratch**_ seems to be a radical thing to do, with so many Content 
Management Systems available. But I have lost some of my PHP & MySQL skills, last
used php4 and MySql3. So this will be an experience to brush up on this. 

I'm not finding a CMS, that I like. The last one that I _REALLY_ enjoyed using was 
XOOPS 2.0.18, I evan have an original 2.0.18-2 in original.

I think that if it gets cleaned up and updated, modernizing the backend and frontend - actually refactoring it,
it would find it's niche somewhere.

**IT IS** a lot easier to use than Drupal or Joomla, and it's not as messy as WordPress.

----

**BUUUUTTTTT**, we are going to build a system (_almost_) _**from scratch**_. We will
use Symfony 3 and use as many components and/or bundles as needed and see wthe System grow!.


Adding general website neccesities to the base, the CORE.

Building  modules, plugins, themes, skins, etc. - whatever we will call them.



## Planned **CORE** FEATURES

* Integreted _PAGES_ with _Catagories_
  * Create that do not change often. Sucha as:
    * Legal Pages
    * Tutorials
    * About Us
  * Any page that doesn't change often.

* EU Cookie Law Compliance
  * No theme dependance

* Captcha and/or reCaptcha

* Contact Page

* Update/Upgrade Method
  * Something like WP has (drupal is ok too)
  
* Bootstrap

* JQuery


  
## Planned Requirements

* PHP 7+
* MySQLi (pdo-mysqli prefered)
* Webserver 
   * Apache2 or nGinX (maybe others)
   * PHP 7
   * MySQL

## Documentation

During the development phases that are HUGE changes, will have most of it's
documentation [here in our wiki](https://gitlab.com/HubShark/MustangX/wikis/home)

Once we start using a release, even if it's still in testing status, are at [the projects website](http://mustangx.org)


## License

MIT License


### Misc

We may add some snippets that may be usefull for the project. This will stay [here in the snippets area](https://gitlab.com/HubShark/MustangX/snippets),

in case a _core, module_ or _theme_ developer needs it.




#### How to Use

We're still in development, so we don't know how we will deploy this. Until then you could get it like this on a development server (pc?):

`git clone https://gitlab.com/HubShark/MustangX.git`

**IMPORTANT!!!** Fix File Permissions (if you are not using Linux check the Symfony instructions). First make sure to change into the project’s directory:

`cd MustangX`

Then fix the permissions like this (this was done & tested on Debian/Ubuntu/Devuan):

```
HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
 
# if this doesn't work, try adding `-n` option
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
```

The next part will ask for Databank Info (you should already have them) and at the end it will ask for setting your "secret". You can get an <ahref"http://nux.net/secret" target="_bank">Online Generated key here</a>.

```
composer install
```

answer any questions and it will or may end with errors message about an unkown databank. Fix that error with this:

```
php bin/console doctrine:database:create
````

And then to make sure we got erverything the first time, we run the _install_ command again:

```
composer install
```


Then Create the database schema

```
php bin/console doctrine:schema:create
```


And now update your database:

```
php bin/console doctrine:schema:update --force
```


Then create your :Admin User_ (Replace _"Admin"_ with your adminuser name):

```
php bin/console fos:user:create Admin --super-admin
```


And then we run this to update everything:

```
composer update --with-dependencies
```


_I HAVE NOT TESTED THIS YET. If you do please post an issue and let us know._